package com.example.audi.uts;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.reflect.Array;

public class MainActivity extends AppCompatActivity {

    //bahan bahan yang di perlukan
    TextView txtkesempatan, txtskor;
    ImageView imgsoal;
    EditText pltisi1, pltisi2, pltisi3, pltisi4, pltisi5, pltisi6, pltisi7;
    Button btnjwb1, btnjwb2, btnjwb3, btnjwb4, btnjwb5, btnjwb6, btnjwb7, btnjwb8;
    soal[] s;
    int kesempatan, jawabanke, skor, panjang, soalke;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // set kesempatan jadi 3 (awal)
        kesempatan = 3;
        //daftar txtkesempatan
        txtkesempatan = (TextView) findViewById(R.id.txtkesempatan);
        //tulis di textview
        txtkesempatan.setText("KESEMPATAN : " + String.valueOf(kesempatan));

        //set skor dimulai dari 0
        skor = 0;
        //daftar skor
        txtskor = (TextView) findViewById(R.id.txtskor);
        // set skor jadi 0 (awal)
        txtskor.setText("SKOR : " + String.valueOf(skor));

        //siapkan soal
        s = new soal[]{
                new soal(1, new String[]{"a", "b", "c"}, new String[]{"a", "b", "c", "d", "e", "f", "g", "h"}),
                new soal(1, new String[]{"d", "e", "f", "g"}, new String[]{"d", "e", "f", "g", "h", "i", "j", "k"})
        };

        //set soalke = 0 agar meload soal ke 1 dalam array soal
        soalke = 0;

        //load soal pertama
        loadsoal(soalke);
    }

    //method load soal
    public void loadsoal(int soalke) {
        //hitung panjang jawaban
        panjang = Array.getLength(s[soalke].getJawab());

        //daftarkan jawaban
        pltisi1 = (EditText) findViewById(R.id.pltisi1);
        pltisi2 = (EditText) findViewById(R.id.pltisi2);
        pltisi3 = (EditText) findViewById(R.id.pltisi3);
        pltisi4 = (EditText) findViewById(R.id.pltisi4);
        pltisi5 = (EditText) findViewById(R.id.pltisi5);
        pltisi6 = (EditText) findViewById(R.id.pltisi6);
        pltisi7 = (EditText) findViewById(R.id.pltisi7);

        //tampung edittext ke array (karena ga bisa dynamic variable cth:pltisi+(i))
        EditText[] kolom = new EditText[]{
                pltisi1,
                pltisi2,
                pltisi3,
                pltisi4,
                pltisi5,
                pltisi6,
                pltisi7
        };

        //looping untuk memunculkan kotak
        for (int i = 0; i < panjang; i++) {
            //memunculkan kotak
            kolom[i].setVisibility(View.VISIBLE);
            //membersihkan isi kotak jika berisi jawaban dari soal sebelumnya
            kolom[i].setText("");
        }

        //daftarkan button
        btnjwb1 = (Button) findViewById(R.id.btnjwb1);
        btnjwb2 = (Button) findViewById(R.id.btnjwb2);
        btnjwb3 = (Button) findViewById(R.id.btnjwb3);
        btnjwb4 = (Button) findViewById(R.id.btnjwb4);
        btnjwb5 = (Button) findViewById(R.id.btnjwb5);
        btnjwb6 = (Button) findViewById(R.id.btnjwb6);
        btnjwb7 = (Button) findViewById(R.id.btnjwb7);
        btnjwb8 = (Button) findViewById(R.id.btnjwb8);

        //tampung button di array agar dynamic
        Button[] jawab = new Button[]{
                btnjwb1,
                btnjwb2,
                btnjwb3,
                btnjwb4,
                btnjwb5,
                btnjwb6,
                btnjwb7,
                btnjwb8
        };

        //tampung jawaban soal ke array
        String[] pilihan = s[soalke].getPilihan();

        //ubah text button sesuai soal
        for (int i = 0; i < 8; i++) {
            jawab[i].setVisibility(View.VISIBLE);
            jawab[i].setText(pilihan[i]);
        }

        //set agar cek jawaban huruf ke 1 terlebih dahulu
        jawabanke = 0;
    }

    //method cek jawaban
    public void cekjawab(View view) {

        //ambil text dari button yang di tekan
        Button b = (Button) view;
        String dipilih = b.getText().toString();

        //tampung jawaban ke array
        String[] jawaban = s[soalke].getJawab();

        //cocokkan jawaban dengan text di button
        if (dipilih.equals(jawaban[jawabanke])) {

            //hilangkan button yang di klik
            b.setVisibility(View.INVISIBLE);

            //daftarkan jawaban
            pltisi1 = (EditText) findViewById(R.id.pltisi1);
            pltisi2 = (EditText) findViewById(R.id.pltisi2);
            pltisi3 = (EditText) findViewById(R.id.pltisi3);
            pltisi4 = (EditText) findViewById(R.id.pltisi4);
            pltisi5 = (EditText) findViewById(R.id.pltisi5);
            pltisi6 = (EditText) findViewById(R.id.pltisi6);
            pltisi7 = (EditText) findViewById(R.id.pltisi7);

            //tampung edittext ke array (karena ga bisa dynamic variable cth:pltisi+(i))
            EditText[] kolom = new EditText[]{
                    pltisi6,
                    pltisi4,
                    pltisi2,
                    pltisi1,
                    pltisi3,
                    pltisi5,
                    pltisi7
            };

            //looping untuk cari kotak yang kosong
            //penanda kapan whilenya bisa berhenti
            boolean kotakkosong = false;
            //counter untuk array kolom
            int counter = 0;
            //looping selama kotakkosong belum ditemukan
            while (kotakkosong == false) {
                //cek kolom ke i visible atau ndak
                boolean tampil = kolom[counter].isShown();
                if (tampil == true) {
                    //kalau visible cek lagi, tu kotak dah ada isinya belom
                    if (kolom[counter].getText().toString().equals("")) {
                        //kalau kosong, isi kotak dengan huruf yang ada di button
                        kolom[counter].setText(dipilih);
                        //kurangi jumlah huruf yang harus di jawab
                        panjang--;
                        //suruh whilenya berhenti dengan set kotak kosong menjadi true
                        kotakkosong = true;
                    } else {
                        //kalau kotaknya isi sesuatu, lanjut cek kotak berikutnya
                        counter++;
                    }
                } else {
                    //kalau kotak tersebut terhidden (ndak visible) lanjut cek kotak berikutnya
                    counter++;
                }
            }

            //mengubah untuk cek huruf berikutnya pada jawaban
            jawabanke++;
        } else {
            //membatasi pengurangan kesempatan biar ga lebih dari 0
            if (kesempatan > 1) {
                //kurangi kesempatan player
                kesempatan--;
                //daftar txtkesempatan
                txtkesempatan = (TextView) findViewById(R.id.txtkesempatan);
                //tulis di textview
                txtkesempatan.setText("KESEMPATAN : " + String.valueOf(kesempatan));
            } else {
                //kalau kesempatannya habis, maka . . ., silahkan di coding sendiri ^^
            }
        }
        //cek apakah masih ada huruf yang harus di jawab ?
        if (panjang == 0) {
            //kalau sudah tidak ada, tambah skor 100 poin
            skor += 100;
            //daftar skor
            txtskor = (TextView) findViewById(R.id.txtskor);
            // set skor di textview
            txtskor.setText("SKOR : " + String.valueOf(skor));
            //cek apakah masih ada soal berikutnya ? dengan cara cek sekarang soal ke berapa bandingkan dengan jumlah array soal - 1
            if(soalke<s.length-1){
                //kalau masih ada, set soalke untuk load soal berikutnya
                soalke++;
                //load soal berikutnya
                loadsoal(soalke);
            } else {
                //kalau soal sudah habis, maka... silahkan di coding sendiri ^^
            }
        }
    }
}
