package com.example.audi.uts;

/**
 * Created by Audi on 3/15/2017.
 */

public class soal {
    private int id;
    private String[] jawab = new String[3];
    private String[] pilihan = new String[3];

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String[] getJawab() {
        return jawab;
    }

    public void setJawab(String[] jawab) {
        this.jawab = jawab;
    }

    public String[] getPilihan() {
        return pilihan;
    }

    public void setPilihan(String[] pilihan) {
        this.pilihan = pilihan;
    }

    public soal(int id, String[]jawab, String[]pilihan){
        setId(id);
        setJawab(jawab);
        setPilihan(pilihan);
    }
}
